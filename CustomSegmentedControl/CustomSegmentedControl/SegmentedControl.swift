//
//  ViewController.swift
//  CustomSegmentedControl
//
//  Created by pradeep burugu on 9/27/16.
//  Copyright © 2016 pradeep burugu. All rights reserved.
//

import UIKit

enum BorderPosition {
    case None
    case Bottom
    case Top
}

class SegmentedControl: UIControl {
    internal var selectedSegmentIndex = 0 {
        didSet {
            previousLabelState(color: defaultTitleColor)
            selectionIndicatorLayer.frame = frameForSelectionIndicator
        }
    }
    
    private var previousSegmentIndex: Int = 0 {
        didSet {
            previousLabelState(color: defaultTitleColor)
        }
    }
    
    internal var defaultTitleColor = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1)
    internal var selectionTitleAndIndicatorColor = UIColor(red: 103.0/255.0, green: 93.0/255.0, blue: 198.0/255.0, alpha: 1)
    internal var selectionIndicatorHeight: CGFloat = 3.0
    internal var labelFontSize: CGFloat = 14.0
    internal var borderPosition: BorderPosition = .None
    
    internal var titles:[String] = [] {
        didSet {
            titles = titles.filter { $0 != "" }
        }
    }
    
    private var totalSegmentsWidth: CGFloat {
        return CGFloat(segmentsCount) * segmentWidth
    }
    
    private var segmentsCount: Int {
        return titles.count
    }
    
    private var segmentWidth: CGFloat {
        return segmentsCount > 0 ? frame.width / CGFloat(segmentsCount) : 0.0
    }
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.scrollsToTop = false
        scrollView.isUserInteractionEnabled = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private var selectionIndicatorLayer = CALayer()
    
    private func setSelected(selectedIndex: Int, animated: Bool) {
        selectedSegmentIndex = selectedIndex
        label[selectedIndex].textColor = selectionTitleAndIndicatorColor
        scrollToSelectedIndex(animated: animated)
        sendActions(for: .valueChanged)
    }
    
    private func previousLabelState(color: UIColor) {
        if !label.isEmpty {
            label[previousSegmentIndex].textColor = color
        }
    }
    
    internal override func layoutSubviews() {
        super.layoutSubviews()
        setupScrollView()
        setupTitles()
        setupSelectionIndicator()
    }
    
    private func setupScrollView() {
        scrollView.contentInset = .zero
        scrollView.frame = CGRect(origin: .zero, size: frame.size)
        scrollView.isScrollEnabled = true
        scrollView.contentSize = CGSize(width: totalSegmentsWidth, height: frame.height)
        addSubview(scrollView)
    }
    
    private func scrollToSelectedIndex(animated: Bool) {
        let rectToScroll: CGRect = {
            var rectToScroll = CGRect(x: segmentWidth * CGFloat(selectedSegmentIndex), y: 0, width:segmentWidth , height: frame.height)
            let scrollOffset = frame.width / 2 - segmentWidth / 2
            rectToScroll.origin.x -= scrollOffset
            rectToScroll.size.width += scrollOffset * 2
            return rectToScroll
        }()
        scrollView.scrollRectToVisible(rectToScroll, animated: animated)
    }
    
    private var label = [UILabel]()
    
}
extension SegmentedControl {
    internal override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        guard let touch: UITouch = touches.first  else { return }
        
        let touchLocation = touch.location(in: self)
        bounds.contains(touchLocation)
        
        previousSegmentIndex = selectedSegmentIndex
        let touchIndex = Int((touchLocation.x) / segmentWidth)
        if touchIndex != selectedSegmentIndex {
            setSelected(selectedIndex: touchIndex, animated: true)
        }
    }
}
extension SegmentedControl {
    
    private func setupTitles() {
        for (index, title) in titles.enumerated() {
            
            var titleLabel: UILabel {
                let label = UILabel()
                label.textColor = defaultTitleColor
                label.font = UIFont.systemFont(ofSize: labelFontSize, weight: .semibold)
                label.text = title
                label.adjustsFontSizeToFitWidth = true
                label.contentScaleFactor = UIScreen.main.scale
                label.textAlignment = .center
                label.frame = CGRect(x: segmentWidth * CGFloat(index) ,y: 13 , width: segmentWidth, height: labelFontSize)
                label.sizeThatFits(UILayoutFittingCompressedSize)
                
                return label
            }
            
            label.append(titleLabel)
            
            scrollView.addSubview(label[index])
            
        }
        
        label[0].textColor = selectionTitleAndIndicatorColor
        
    }
    
    private func setupSelectionIndicator() {
        selectionIndicatorLayer.backgroundColor = selectionTitleAndIndicatorColor.cgColor
        selectionIndicatorLayer.frame = frameForSelectionIndicator
        scrollView.layer.addSublayer(selectionIndicatorLayer)
    }
    
    private var frameForSelectionIndicator: CGRect {
        let xPosition: CGFloat
        let yPosition: CGFloat
        let indicatorHeight: CGFloat
        let indicatorWidth: CGFloat
        
        switch borderPosition {
        case .Bottom:
             xPosition = segmentWidth * CGFloat(selectedSegmentIndex)
             yPosition = frame.height - selectionIndicatorHeight
             indicatorHeight = selectionIndicatorHeight
             indicatorWidth = segmentWidth
        case .None:
             xPosition = 0.0
             yPosition = 0.0
             indicatorHeight = 0.0
             indicatorWidth = 0.0
        case .Top:
            xPosition = segmentWidth * CGFloat(selectedSegmentIndex)
            yPosition = 0.0
            indicatorHeight = selectionIndicatorHeight
            indicatorWidth = segmentWidth
        }
    
        previousSegmentIndex = selectedSegmentIndex
        previousLabelState(color: selectionTitleAndIndicatorColor)
        
        let fullRect = CGRect(x: xPosition, y: yPosition, width: indicatorWidth, height: indicatorHeight)
        
        return fullRect
    }
    
}

