//
//  ViewController.swift
//  CustomSegmentedControl
//
//  Created by pradeep burugu on 9/27/16.
//  Copyright © 2016 pradeep burugu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var segmentedControl: SegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        segmentedControl.titles = ["first", "second", "third"]
        segmentedControl.borderPosition = .Top
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

